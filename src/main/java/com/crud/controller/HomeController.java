package com.crud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.bean.PersonBean;
import com.crud.dao.PersonDao;
import com.crud.entity.Person;

@RestController
@RequestMapping("/home")
public class HomeController {

	@Autowired
	private PersonDao personDao;

	@PostMapping("/saveperson")
	public Person savePerson(@RequestBody PersonBean personBean) {
		return personDao.savePerson(personBean);
	}

}
