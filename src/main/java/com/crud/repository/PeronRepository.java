package com.crud.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.crud.entity.Person;

@Repository
@Transactional
public interface PeronRepository extends CrudRepository<Person, Long> {
	
	

}
