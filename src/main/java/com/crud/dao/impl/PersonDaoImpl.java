package com.crud.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.bean.PersonBean;
import com.crud.dao.PersonDao;
import com.crud.entity.Person;
import com.crud.repository.PeronRepository;

@Service
public class PersonDaoImpl implements PersonDao {

	@Autowired
	private PeronRepository peronRepository;

	@Override
	public Person savePerson(PersonBean personBean) {
		// TODO Auto-generated method stub
		Person person = new Person();
		person.setAge(personBean.getAge());
		person.setName(personBean.getName());
		person = peronRepository.save(person);
		return person;
	}

}
