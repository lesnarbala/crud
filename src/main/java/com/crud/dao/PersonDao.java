package com.crud.dao;

import com.crud.bean.PersonBean;
import com.crud.entity.Person;

public interface PersonDao {

	Person savePerson(PersonBean personBean);

}
